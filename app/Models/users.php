<?php
namespace App\Models;
use \Illuminate\Database\Eloquent\Model;

class Users extends Model {
    protected $table        =   'users';
    protected $fillable     =   ['name','email','phone'];
    public $timestamps      =   false;
    public $data;
    
    public function checkUsersbyMobile()
    {
        $users  =   $this->wherephone($this->data['mobile'])->first();
        if($users)
        {
            $user =    $users->getOriginal();
            return $this->id    =   $user['id'];
            
        }
        return  false;
    }
    
    public function insertUser($data)
    {
        $this->data =   $data;
       // if(empty($this->checkUsersbyMobile()))
        //{
            $this->name         =   $this->data['name'];
            $this->email        =   $this->data['email'];
            $this->phone        =   $this->data['mobile'];
            $this->utm_source   =   $this->data['utm_source'];
            $this->utm_medium   =   $this->data['utm_medium'];
            $this->utm_term     =   $this->data['utm_term'];
            $this->added_on     =   date('Y-m-d');
            $this->save();
        //}
        return  $this->userRecordById();
    }
    
    public function userRecordById()
    {
        return  $this->whereid($this->id)->first()->getOriginal();
    }
    public function getUsers($limit=10)
    {
       
        $this->q            =   $this->select('*');
        $this->setSearch();
        $this->totalCount   =   $this->q->count();
        $this->setLimit();
        $this->recordOrderBy();

        $q  = $this->get();

        
        foreach($q as $key=>$value)
        {
            $row    =   $value->getOriginal();
           
            $result[]   =   $row;
        }
        return $result;
    }
    private function recordOrderBy()
    {
        $this->q->orderBy('id','ASC');
    }
    
    private function setSearch()
    {
        //$this->q->where(['name'=>$name]);
    }
    private function setLimit()
    {
        if(isset($_REQUEST['page']) && !empty($_REQUEST['page']))
        {
            $start  =   $_REQUEST['page']*$this->perPageLimit;
        }
        else
        {
            $start=0;
        }
        $this->q->skip($start)->take($this->perPageLimit);
    }

    public function getUsersCSV()
    {
        
        $this->q            =   $this->with('userLeaderBoard')->orderBy('id','ASC');
        $q  = $this->q->get();
        
        foreach($q as $key=>$value)
        {
            $row        =   $value->getOriginal();
            $result[]   =   $row;
        }
        return $result;
    }
    
}