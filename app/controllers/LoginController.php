<?php

namespace App\Controllers;
use App\Models\Users;
use App\Models\Admin;
class LoginController
{
    /**
     * Show the home page.
     */
    public function __construct()
    {
        
    }
    
    public function index()
    {
        if($data=checkPostRequest())
        {   
            
             $result = Admin::whereusername($data['username'])->wherePassword(md5($data['password']))->first()->getOriginal();
             if($result)
             {
                 setSession('admin',$result);
                 redirect('admin/user');
             }
             return view('admin/login', ['msg' => 'Invalid Credential']);
        }
        return view('admin/login');
        
    }

   
    public function about()
    {
        $company = 'Laracasts';

        return view('about', ['company' => $company]);
    }

  
    public function contact()
    {
        return view('contact');
    }
}
