<?php

namespace App\Controllers;
use App\Models\Users ;
class PagesController
{
    public function __construct(){
       
       if(checkCookies('users'))
       {
         redirect('game');
       }
        
    }
    
    public function index()
    {
        $currentLeader='';
        if($data=checkPostRequest())
        {
            $user       =   new Users();
            $userdata   =   $user->insertUser($data);
            setCookies('users',$userdata);
            //redirect('game');
        }
        
        
        return view('index',[]);
    }
}
