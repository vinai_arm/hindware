 <?php
//$subfolder='tick/oppo_quiz_game';
$router->get(SUB_PATH.'/admin', 'LoginController@index');
$router->post(SUB_PATH.'/admin', 'LoginController@index');
$router->get(SUB_PATH.'/admin/user','UsersController@index');
$router->get(SUB_PATH.'/admin/user/leaderboard','UsersController@userLeaderBoard');
$router->get(SUB_PATH.'', 'PagesController@index');
$router->post(SUB_PATH.'', 'PagesController@index');

$router->get(SUB_PATH.'/game','GamesController@index');
$router->post(SUB_PATH.'/game','GamesController@saveAnswer');
$router->get('users', 'UsersController@index');
$router->post('users', 'UsersController@store');
$router->get(SUB_PATH.'/admin/question','QuestionsController@index');
$router->get(SUB_PATH.'/admin/uploadQuestionSheet','QuestionsController@uploadQuestionSheet');
$router->post(SUB_PATH.'/admin/leaderboard','UsersController@LeaderBoard');
$router->get(SUB_PATH.'/admin/allUsers','UsersController@getUsers');
$router->get(SUB_PATH.'/admin/downloadcsv','UsersController@leaderBoardCSV');
$router->get(SUB_PATH.'/admin/downloadUsercsv','UsersController@UserCSV');