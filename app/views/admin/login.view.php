<?php 
$BASE_URL='http://localhost/wtc/';
$PUBLIC_FILE_URL=$BASE_URL.'public/';

?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <meta charset="utf-8">
  <title>Admin Login </title>
  <meta name="viewport" content="width=device-width, initial-scale=1.0">

  <link href="<?php echo $PUBLIC_FILE_URL;?>css/bootstrap.css" rel="stylesheet">
  <link rel="stylesheet" href="<?php echo $PUBLIC_FILE_URL;?>css/font-awesome.css">
  <link href="<?php echo $PUBLIC_FILE_URL;?>css/style.css" rel="stylesheet">
  
  <!-- HTML5 Support for IE -->
  <!--[if lt IE 9]>
  <script src="js/html5shim.js"></script>
  <![endif]-->

  <link rel="shortcut icon" href="<?php echo $PUBLIC_FILE_URL; ?>css/img/favicon/favicon.png">
</head>

<body>

<!-- Form area -->
<div class="admin-form">
  <div class="container">

    <div class="row">
      <div class="col-md-12">
        <!-- Widget starts -->
            <div class="widget">
              <!-- Widget head -->
              <div class="widget-head">
                <i class="icon-lock"></i> Login 
              </div>

              <div class="widget-content">
                <div class="padd">
                  <!-- Login form -->
                  <form class="form-horizontal" action='' method='post'>
					
                  <!-- Email -->
                  <div class="form-group">
                    <label class="control-label col-lg-3" for="inputEmail">Username</label>
                    <div class="col-lg-9">
                    <input type="text" name="username" class="form-control" id="inputEmail" placeholder="Username">
                    </div>
                  </div>
                  <!-- Password -->
                  <div class="form-group">
                    <label class="control-label col-lg-3" for="inputPassword">Password</label>
                    <div class="col-lg-9">
                    <input type="password" name="password" class="form-control" id="inputPassword" placeholder="Password">
                    </div>
                  </div>
                  <div class="form-group">
                  </div>
                    <div class="col-lg-9 col-lg-offset-3">
                      <button type="submit" class="btn btn-danger btn-lg" style="margin-left:-10px;">LOGIN</button>
                    </div>
						      <br />
                  <br />
                  </form>
                  <div class="clearfix"></div>
				  
				</div>
                </div>
              
                <div class="widget-foot">
				<center><font color="red" size="2px"></font></center>
                </div>
            </div>  
      </div>
    </div>
  </div> 
</div>
	
		

<!-- JS -->
<script src="<?php echo $PUBLIC_FILE_URL; ?>js/bootstrap.js"></script>
</body>

</html>