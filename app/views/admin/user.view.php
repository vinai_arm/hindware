<?php include('include/header.php'); ?>
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.css">
<div class="mainbar">
		<div class="matter">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<div class="widget">
							<div class="widget-head">
								<div class="pull-left">Manage Users</div>
								<div class="widget-icons pull-right">
								<a href="#" class="wminimize"></a>
								</div>
                                <a class="btn btn-primary ch2" href="<?php echo $BASE_URL;?>admin/downloadUsercsv">download CSV</a>
								<div class="clearfix"></div>
							</div>

							<div class="widget-content" style="margin: 0px;width:100%;">
                            <div class="table-responsive">
  								<table id="datatable" class="table table-striped table-bordered table-hover">
  									  <thead>
                                            <tr>
                                                <th>Name</th>
                                                <th>Email</th>
                                                <th>Phone</th>
                                                <th>UTM Source</th>
                                                <th>UTM Medium</th>
                                                <th>UTM Term</th>
    										</tr>
  									  </thead>
                                    <tbody>
                                       <?php
                                        if(isset($users))
                                        {
                                            foreach($users as $key=>$value)
                                            {
                                                ?>
                                                <tr>
                                                    <th><?php echo $value['name']; ?></th>
                                                    <th><?php echo $value['email']; ?></th>
                                                    <th><?php echo $value['phone']; ?></th>
                                                    <th><?php echo $value['utm_source']; ?></th>
                                                    <th><?php echo $value['utm_medium']; ?></th>
                                                    <th><?php echo $value['utm_term']; ?></th>
                                                </tr>
                                                <?php
                                            }
                                        }
                                        ?>
                                    
                                    </tbody>
  								</table>
                                </div>
								<div class="widget-foot">
								  <br><br>
									<div class="clearfix"></div>
								</div>
							  </div>
						</div>
					</div>
				</div>
			</div>
		</div>
    </div>
   <div class="clearfix"></div>
</div>
<?php include('include/footer.php'); ?>
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.js"></script>
<script>
    $(document).ready( function () {
        $('#datatable').DataTable();
        /*
        $('#datatable').DataTable({
		 "bProcessing": true,
         "serverSide": true,
         "ajax":{
            url :"<?php echo $BASE_URL;?>admin/allUsers", 
            type: "get",  
            error: function(){  
              $("#employee_grid_processing").css("display","none");
            }
          }
        });   
        */
    });
</script>