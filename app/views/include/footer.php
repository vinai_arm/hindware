 <footer>
    <div class="container">
      <div class="row">
        <div class="footer-top">
          <div class="col-md-4">
            <div class="footer-text">
              <dl>
                <dt>
                  <a href="https://www.oppo.com/in/smartphones/" target="_self">Smartphones</a><img class="expandtiny" src="https://assorted.downloads.oppo.com/static/archives/images/en/svg/expandtiny.svg">
                </dt>
                <dd><a href="https://www.oppo.com/in/smartphone-f9/" target="_self">F9</a></dd>
                <dd><a href="https://www.oppo.com/in/smartphone-f9-pro/" target="_self">F9 Pro (India)</a></dd>
                <dd><a href="https://www.oppo.com/in/smartphone-find_x/" target="_self">Find X</a></dd>
                <dd><a href="https://www.oppo.com/in/smartphone-a5/" target="_self">A5</a></dd>
                <dd><a href="https://www.oppo.com/in/smartphone-a3s/" target="_self">A3s</a></dd>
              </dl>

            </div>
          </div>
          <div class="col-md-4">
            <div class="footer-text">
              <dl>
                <dt>
                  <a href="https://oppo-in.custhelp.com/ " target="_self">Support</a><img class="expandtiny" src="https://assorted.downloads.oppo.com/static/archives/images/en/svg/expandtiny.svg">
                </dt>
                <dd><a href="https://oppo-in.custhelp.com/app/contactoppo/contacts" target="_self">Contact Us</a></dd>
                <dd><a href="https://www.oppo.com/in/about-us/press/" target="_self">Press</a></dd>
                <dd><a href="https://security.oppo.com/" target="_blank">Security Bug Bounty</a></dd>
                <dd><a href="https://www.oppo.com/in/about-us/press/jio" target="_blank">JIO OPPO Instant Cashback Offer</a></dd>
                <dd><a href="https://www.oppo.com/in/about-us/press/e-waste-management/" target="_self">E-waste Management</a></dd>
              </dl>
            </div>

          </div>
          <div class="clearfix"></div>

          <div class="col-md-12 mob-none">
            <div class="footer_country">
              <a href="https://www.oppo.com/in/global/">India</a>
            </div>


          </div>
          <div class="clearfix"></div>

        </div>
		<div class="fot_mobile">
		<div class="col-md-8 col-sm-8 col-xs-8"><div class="copy_m">Copyright © 2018 OPPO. All rights reserved. </div></div>
		<div class="col-md-4 col-sm-4 col-xs-4"><div class="c_mob"><a href="https://www.oppo.com/in/global/">India</a></div></div>
		<div class="clearfix"></div>
		</div>

        <div class="footer_bottom">
          <div class="col-md-6 mob-none">
            <div class="copy">Copyright © 2018 OPPO. All rights reserved. </div>

          </div>
          <div class="col-md-6">
            <div class="social">
              <ul>
                <li>Follow us:</li>
                <li><a href="https://www.facebook.com/oppomobileindia" target="_blank"><img src="//assorted.downloads.oppo.com/static/archives/images/en/Social%20Media%20Icons/NEW_UI/facebook.svg" alt="facebook"></a></li>
                <li><a href="https://www.instagram.com/oppomobileindia/" target="_blank"><img src="//assorted.downloads.oppo.com/static/archives/images/en/Social%20Media%20Icons/NEW_UI/instagram.svg" alt="instagram"></a></li>
                <li><a href="https://twitter.com/oppomobileindia" target="_blank"><img src="//assorted.downloads.oppo.com/static/archives/images/en/Social%20Media%20Icons/NEW_UI/Twitter.svg" alt="twitter"></a></li>
                <li><a href="https://www.youtube.com/user/oppomobileindia" target="_blank"><img src="//assorted.downloads.oppo.com/static/archives/images/en/Social%20Media%20Icons/NEW_UI/youtube.svg" alt="youtube"></a></li>
              </ul>


            </div>
          </div>
        </div>





      </div>
    </div>
  </footer>


<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script src='https://cdn.jsdelivr.net/npm/jquery-validation@1.17.0/dist/jquery.validate.js'></script>
<script src='<?php echo $PUBLIC_FILE_URL; ?>js/jquery.easing.1.3.js'></script>
<script src='<?php echo $PUBLIC_FILE_URL; ?>js/comman.js'></script>
</body>

</html>