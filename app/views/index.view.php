<?php include('include/header.php'); ?>
    <section class="play-Quiz">
        <div class="container">
            <div class="row">
               
                 <div class="col-md-12 float-none mrg-auto registerfrom">
                    <div class="register text-center sec-padding">
                        <h3 class="game-heading fnt-70">Register</h3>
						<div class="row">
						<div class="col-md-6 float-none mrg-auto">
                        <form action="" method="post" id='registerform'>
                            <input type="hidden" value="<?php echo isset($_GET['utm_source'])?$_GET['utm_source']:'';?>" name="utm_source">
                            <input type="hidden" value="<?php echo isset($_GET['utm_medium'])?$_GET['utm_medium']:'';?>" name="utm_medium">
                            <input type="hidden" value="<?php echo isset($_GET['utm_term'])?$_GET['utm_term']:'';?>" name="utm_term">
                            <div class="form-group">
                                <input type="text" class="form-control alpha required" id="name" placeholder="Name" name="name">
                            </div>
							<div class="form-group">
                                <input type="text" class="form-control required digits phone" maxlength="10" minlength='10' id="mobile" placeholder="Phone Number" name="mobile">
                            </div>
                            <div class="form-group">
                                <input type="email" class="form-control email" id="email" placeholder="Email Id" name="email">
                            </div>
                            
                            <div class="form-group">
                            <input type="submit" class="submit-info" value="submit">
                            </div>
                        </form>
						</div>
						</div>
                    </div>
                </div>

              
                <!--Top Scorer-->
                <div class="col-md-12">
                    
                    <div class="t-and-c text-center">
                        <!--<p>
                            <a href="<?php echo $PUBLIC_FILE_URL; ?>pdfs/Terms-and-Conditions.pdf" target="_blank">Terms and Conditions apply</a>
                        </p>-->
                    </div>
                </div>
            </div>
        </div>
    </section>
<?php //include('include/footer.php'); ?>
<script>
jQuery('#registerform').validate();
jQuery.validator.addMethod("phone", function(phone_number, element) {
    phone_number = phone_number.replace(/\s+/g, ""); 
    return this.optional(element) || phone_number.match(/^[6-9]\d{9}$/);
}, "Please Enter Valid Phone Number");
jQuery('.play-now').click(function(e){
               
        $('.banner').hide();
        $('.registerfrom').show();
 });
$.validator.addMethod("alpha", function(value, element) {
    return this.optional(element) || value == value.match(/^[a-zA-Z ]*$/);
}, "Please Enter Valid Name");
</script>
    