<?php

/**
 * Require a view.
 *
 * @param  string $name
 * @param  array  $data
 */
function view($name, $data = [])
{
    extract($data);

    return require "app/views/{$name}.view.php";
}

/**
 * Redirect to a new page.
 *
 * @param  string $path
 */
function redirect($path)
{
    if(SUB_PATH){
        header("Location: /".SUB_PATH."/{$path}");
    }
    else
    {
        header("Location: /".SUB_PATH."/{$path}"); 
    }
    exit;
}


function checkSession($key)
{
    if(isset($_SESSION[$key]))
    {
        return true;
    }
    return false;
}

function checkCookies($key)
{ 
    if((isset($_COOKIE[$key])) && !empty($_COOKIE[$key]) )
    {
        return $_COOKIE[$key];
    }
    return false;
}

function setSession($key,$sessionarray=array())
{
   $_SESSION[$key]=$sessionarray;
}

function setCookies($key,$cookiearray)
{
    setcookie($key, json_encode($cookiearray), time() + (1800), "/"); 
     
}

function checkPostRequest()
{
    if(isset($_POST) && !empty($_POST))
    {
        return $_POST;
    }
    return false;
}

function ddd($array)
{
    
    echo '<pre>'; print_R($array); echo '</pre>'; 
    die;
}

function d($array)
{
    echo '<pre>'; print_R($array); echo '</pre>'; 
   
}
