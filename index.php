<?php
session_start();
ini_set('display_errors',1);
require 'core/bootstrap.php';
define('ROOT_PATH',getcwd());
$BASEPATH = parse_ini_file(ROOT_PATH."/config.ini",true);
define('SUB_PATH',$BASEPATH['DIR']['SUB']);
define('BASE_URL',$BASEPATH['DIR']['BASE_URL']);
use App\Core\{Router, Request};
use App\Models\Database ;
$db=new Database();
Router::load('app/routes.php')
    ->direct(Request::uri(), Request::method());
